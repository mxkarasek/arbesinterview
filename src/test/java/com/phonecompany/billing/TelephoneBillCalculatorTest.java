package com.phonecompany.billing;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class TelephoneBillCalculatorTest {


    @Test
    void calculate_EmptyString() {
        TelephoneBillCalculator instance = new TelephoneBillCalculatorImpl();

        assertEquals(BigDecimal.ZERO, instance.calculate(""));
        assertEquals(BigDecimal.ZERO, instance.calculate(null));
    }

    @Test
    void calculate_oneEntryOutsideMainIntervalShorterThanMaxTime() {
        TelephoneBillCalculator instance = new TelephoneBillCalculatorImpl();

        assertEquals(new BigDecimal("1.50"), instance.calculate("420774577453,13-01-2020 18:10:15,13-01-2020 18:12:57\n" +
                "420111111111,13-01-2020 19:10:15,13-01-2020 19:12:57"));
    }

    @Test
    void calculate_oneEntryInsideMainIntervalShorterThanMaxTime() {
        TelephoneBillCalculator instance = new TelephoneBillCalculatorImpl();

        assertEquals(new BigDecimal("3.00"), instance.calculate("420774577453,13-01-2020 14:10:15,13-01-2020 14:12:57\n" +
                "420111111111,13-01-2020 19:10:15,13-01-2020 19:12:57"));
    }

    @Test
    void calculate_oneEntryOutsideMainIntervalLongerThanMaxTime() {
        TelephoneBillCalculator instance = new TelephoneBillCalculatorImpl();

        assertEquals(new BigDecimal("2.50").add(new BigDecimal("55").multiply(new BigDecimal("0.20"))),
                instance.calculate("420774577453,13-01-2020 18:10:15,13-01-2020 19:10:14\n" +
                        "420111111111,13-01-2020 19:10:15,13-01-2020 19:12:57"));
    }

    @Test
    void calculate_multipleEntriesInsideAndOutside() {
        TelephoneBillCalculator instance = new TelephoneBillCalculatorImpl();

        assertEquals(new BigDecimal("4.50"), instance.calculate(
                "420774577453,13-01-2020 18:10:15,13-01-2020 18:12:57\n" +
                "420774577452,13-01-2020 14:10:15,13-01-2020 14:12:57\n" +
                        "420111111111,13-01-2020 19:10:15,13-01-2020 19:12:57"));
    }

    @Test
    void calculate_multipleEntriesConsideringMostUsedNumber() {
        TelephoneBillCalculator instance = new TelephoneBillCalculatorImpl();

        assertEquals(new BigDecimal("3.00"), instance.calculate(
                "420774577453,13-01-2020 18:10:15,13-01-2020 18:12:57\n" +
                "420774577453,13-01-2020 14:10:15,13-01-2020 14:12:57\n" +
                "420774577455,13-01-2020 14:10:15,13-01-2020 14:12:57"));
    }
}