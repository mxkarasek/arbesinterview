package com.phonecompany.billing;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TelephoneBillCalculatorImpl implements TelephoneBillCalculator {

    private final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    private final static LocalTime TIME_OF_MAIN_INTERVAL_START = LocalTime.of(8, 0, 0);

    private final static LocalTime TIME_OF_MAIN_INTERVAL_END = LocalTime.of(16, 0, 0);

    public BigDecimal calculate(String phoneLog) {
        if (phoneLog == null || phoneLog.isBlank()) {
            return BigDecimal.ZERO;
        }

        List<PhoneBillEntry> phoneBillEntries = parseEntries(phoneLog).collect(Collectors.toList());
        String mostUsedNumber = findMostUsedNumber(phoneBillEntries);

        return phoneBillEntries.stream()
                .filter(entry -> !entry.number.equals(mostUsedNumber))
                .map(this::calculateEntryPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private Stream<PhoneBillEntry> parseEntries(String entries) {
        return Arrays.stream(entries.split("\\n")).map(this::parseEntry);
    }

    private PhoneBillEntry parseEntry(String entryRow) {
        String tokens[] = entryRow.split(",");

        if (tokens.length != 3) {
            throw new IllegalArgumentException("Unexpected entryRow " + entryRow);
        }

        return new PhoneBillEntry(tokens[0],
                LocalDateTime.parse(tokens[1], DATE_TIME_FORMATTER),
                LocalDateTime.parse(tokens[2], DATE_TIME_FORMATTER));
    }

    private BigDecimal calculateEntryPrice(PhoneBillEntry entry) {
        LocalDateTime curPos = entry.start;
        int minuteCounter = 0;
        BigDecimal price = BigDecimal.ZERO;
        do {
            minuteCounter++;
            if (minuteCounter <= 5) {
                if (curPos.toLocalTime().compareTo(TIME_OF_MAIN_INTERVAL_START) >= 0
                        && curPos.toLocalTime().isBefore(TIME_OF_MAIN_INTERVAL_END)) {
                    price = price.add(new BigDecimal("1.00"));
                } else {
                    price = price.add(new BigDecimal("0.50"));
                }
            } else {
                price = price.add(new BigDecimal("0.20"));
            }
            curPos = curPos.plus(Duration.ofMinutes(1));
        } while (curPos.isBefore(entry.end));

        return price;
    }

    private String findMostUsedNumber(List<PhoneBillEntry> phoneBillEntries) {

        Map<String, Integer> numberCounters = new HashMap<>();
        int maxNumber = 0;
        for (PhoneBillEntry entry : phoneBillEntries) {
            Integer value = numberCounters.get(entry.number);
            if (value != null) {
                value = value + 1;
                numberCounters.replace(entry.number, value);
            } else {
                value = 1;
                numberCounters.put(entry.number, value);
            }

            if (value > maxNumber) {
                maxNumber = value;
            }
        }

        String mostUsedNumber = "420999999999";
        for (String phoneNumber : numberCounters.keySet()) {
            if (numberCounters.get(phoneNumber) == maxNumber && phoneNumber.compareTo(mostUsedNumber) < 0) {
                mostUsedNumber = phoneNumber;
            }
        }

        return mostUsedNumber;
    }
}
