package com.phonecompany.billing;

import java.time.LocalDateTime;

public class PhoneBillEntry {

    String number;

    LocalDateTime start;

    LocalDateTime end;

    public PhoneBillEntry(String number, LocalDateTime start, LocalDateTime end) {
        this.number = number;
        this.start = start;
        this.end = end;
    }


}
